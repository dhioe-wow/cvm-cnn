/*
Description: ...
             
Input:
    - wx-bq-poc.loyalty.lylty_card_detail
    - wx-bq-poc.loyalty.article_master
    - wx-bq-poc.loyalty.article_sales_summary
    
Output:
    - A table of CRN, txn_date (Monday) and total weekly spend
*/

DECLARE start_date DATE;
DECLARE end_date DATE;
DECLARE x INT64 DEFAULT 0;
DECLARE y INT64;
SET start_date = '2020-12-01';
SET end_date = '2020-12-31';

CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_unpivoted`
OPTIONS(
  expiration_timestamp=TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 2 HOUR)
) AS 
    WITH prod_code AS
    (
       SELECT 
            prod_nbr, 
            REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(lower(ctgry_name), "[-&()/ ,.'']", "_"), '_+', '_'), '^_', '') AS ctgry_name
        FROM `wx-bq-poc.loyalty.article_master`
        WHERE ctgry_name IS NOT NULL
            AND REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(lower(ctgry_name), "[-&()/ ,.'']", "_"), '_+', '_'), '^_', '') <> ''
        GROUP BY 1,2
    )
    SELECT 
        lcd.crn, 
        DATE_TRUNC(start_txn_date, WEEK(MONDAY)) as txn_date,
        pc.ctgry_name, 
        SUM(ass.tot_amt_incld_gst) AS spend
        -- SUM(ass.prod_sell_qty) AS freq
    FROM `wx-bq-poc.loyalty.article_sales_summary` ass
        LEFT JOIN prod_code pc
            ON ass.prod_nbr = pc.prod_nbr
        LEFT JOIN `wx-bq-poc.loyalty.lylty_card_detail` lcd
            ON ass.lylty_card_nbr = lcd.lylty_card_nbr
    WHERE ass.lylty_card_nbr <> '0'
        AND lcd.crn IS NOT NULL
        AND start_txn_date BETWEEN start_date AND end_date-1
    GROUP BY 1,2,3;
