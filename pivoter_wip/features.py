import os
import time
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json" 
from google.cloud import bigquery


def record_time(f):
    def _record_time(self, *args, **kwargs):            
        t = time.time()
        f(self, *args, **kwargs)
        print(f'Time taken = {time.time()-t:.1f} sec')
    return _record_time


class FeatureProcessor():
    
    def __init__(self, features_count_per_part:int=300):
        self.bq = bigquery.Client(project='gcp-wow-rwds-ai-mmm-super-dev')
        self.sql_features = 'features.sql'
        self.sql_target = 'target.sql'
        self.sql_pivoter = 'ctgry_pivoter.sql'
        self.features_count_per_part = features_count_per_part
        
        
    def run_sql_file(self, sql_file):
        with open(sql_file) as file:
            sql = file.read()
        results = self.bq.query(sql).result()
        
    @record_time
    def get_features_unpivoted(self):
        self.run_sql_file(self.sql_features)
        
        
    def get_pivoter(self):
        self.run_sql_file(self.sql_pivoter)
        
            
    def get_ctgry_count(self):
        sql = '''
            SELECT 
                CAST(COUNT(DISTINCT ctgry_name) AS INT64) AS ctgry_count,
                CAST(CEILING(COUNT(DISTINCT ctgry_name)/{}) AS INT64) AS part_count
            FROM `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_unpivoted`
        '''.format(self.features_count_per_part)

        results = self.bq.query(sql).result()
        for result in results:
            ctgry_count = result.ctgry_count
            part_count = result.part_count
            
        return ctgry_count, part_count


    def _get_features_pivoted_part(self, part_count):
        # TODO: This should be cast as a multiprocessing task
        
        print(f"Number of parts = {part_count}")
        for i in range(part_count):
            sql = '''
                CALL `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_pivoter`(
                    'gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_unpivoted', {length}, {i});
            '''.format(length=self.features_count_per_part, i=i)
            print(sql)
            t = time.time()
            self.bq.query(sql).result()
            print(f'Time taken = {time.time() - t:.1f} sec')
             
    
    @record_time
    def get_features_pivoted(self, part_count):
        self._get_features_pivoted_part(part_count)
        
        base_sql = '''
            CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_pivoted` AS
            SELECT part_0.*
                {sql_select}
            FROM
                gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_pivoted_0 AS part_0
                {sql_from}
        '''
        
        sql_select = []
        sql_from = []
        for i in range(1, part_count):
            sql_select.append(f', part_{i}.* EXCEPT(crn)')
            sql_from.append(f'LEFT JOIN gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_pivoted_{i} AS part_{i} ON part_0.crn = part_{i}.crn')
        sql_select = '\n'.join(sql_select)
        sql_from = '\n'.join(sql_from)
            
        sql = base_sql.format(sql_select=sql_select, sql_from=sql_from)        
        self.bq.query(sql).result()

        
    def execute(self):
        
        t = time.time()
        
        # refresh pivoter
        self.get_pivoter()
        
        print("==== Getting features (unpivoted) table into gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_unpivoted...")
        self.get_features_unpivoted()        
        ctgry_count, part_count = self.get_ctgry_count()
        print(f"Number of categories = {ctgry_count}")
        print("==== Getting features (pivoted) table into gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_pivoted...")
        self.get_features_pivoted(part_count)

        print("==== Completed.")
        print(f'Total time taken = {time.time()-t:.1f} sec')
        
        

if __name__ == '__main__':
    
    feature_processor = FeatureProcessor()
    feature_processor.execute()
    
    
#     import pdb; pdb.set_trace()