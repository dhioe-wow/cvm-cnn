CREATE OR REPLACE PROCEDURE `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.ctgry_spend_pivoter`(
    table_name STRING,
    cat_list_len INT64,
    cat_list_idx INT64
)

BEGIN
    
    DECLARE cat_list_str STRING;
    DECLARE cat_list_idx_100 INT64;
    
    SET cat_list_idx_100 = cat_list_idx * 100;

    EXECUTE IMMEDIATE FORMAT("""
        SELECT
            CONCAT(
                '("',
                STRING_AGG
                (
                    ctgry_name, '", "'
                ),
                '")'
            ) AS cat_list_str
        FROM 
        (
            SELECT DISTINCT ctgry_name
            FROM %s
            WHERE ctgry_name <> ''
            ORDER BY ctgry_name
            LIMIT %d OFFSET %d
        );
          """, table_name, cat_list_len, cat_list_idx_100)
    INTO cat_list_str;
    
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '1', 'one');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '2', 'two');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '3', 'thr');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '4', 'fou');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '5', 'fiv');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '6', 'six');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '7', 'sev');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '8', 'eig');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '9', 'nin');
    SET cat_list_str = REGEXP_REPLACE(cat_list_str, '0', 'zer');

    EXECUTE IMMEDIATE FORMAT("""
        CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-super-dev.MMM_DEV.ctgry_spend_pivoted_%s`
        OPTIONS(
            expiration_timestamp=TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 2 HOUR)
        ) AS 
            SELECT * 
            FROM (SELECT * FROM %s)
            PIVOT (SUM(spend) FOR ctgry_name IN %s)
        ;
        """, CAST(cat_list_idx AS STRING), table_name, cat_list_str);
    
END;